#!/usr/bin/python
# -*- coding: utf-8 -*-
# This is a standalone program. Pass an image name as a first parameter of the program.

# todo
# colocar no código os cantos das imagens

import sys
import os
# para poder rotacionar a imagem facilmente
import Image
from math import sin,cos,sqrt
from opencv.cv import *
from opencv.highgui import *

# toggle between CV_HOUGH_STANDARD and CV_HOUGH_PROBILISTIC
USE_STANDARD=0


def oper(img, rect):
	x = 10
	cvSetZero(img)
	iluv = cvCreateMat(img.rows, img.cols, CV_8UC3)
	lum  = cvCreateMat(img.rows, img.cols, CV_8UC1)
	uchan = cvCreateMat(img.rows, img.cols, CV_8UC1)
	val   = cvCreateMat(img.rows, img.cols, CV_8UC1)
# CvtColor -> conversão para LUV
	cvCvtColor(img,iluv,CV_BGR2Luv);
	cvSplit(iluv, lum, uchan, val, None);
	x = cvAvg(lum);
	return x

if __name__ == "__main__":
	filename = "samples/dscf1447.jpg"
	if len(sys.argv)>1:
		filename = sys.argv[1]

	src=cvLoadImage(filename, 0);
	if not src:
		print "Error opening image %s" % filename
		sys.exit(-1)

	dst = cvCreateImage( cvGetSize(src), 8, 1 );
	color_dst = cvCreateImage( cvGetSize(src), 8, 3 );
	storage = cvCreateMemStorage(0);
	lines = 0;
	graus = [0, 0, 0, 0];
	cvCanny( src, dst, 50, 200, 3 );
	cvCvtColor( dst, color_dst, CV_GRAY2BGR );

	if USE_STANDARD:
		lines = cvHoughLines2( dst, storage, CV_HOUGH_STANDARD, 1, CV_PI/180, 100, 0, 0 );
		log = file(r"/tmp/log.txt", "a+");
		for i in range(min(lines.total, 100)):
			line = lines[i]
			rho = line[0];
			theta = line[1];
			log.write(str(theta) + '\n');
			pt1 = CvPoint();
			pt2 = CvPoint();
			a = cos(theta);
			b = sin(theta);
			x0 = a*rho 
			y0 = b*rho
			pt1.x = cvRound(x0 + 1000*(-b));
			pt1.y = cvRound(y0 + 1000*(a));
			pt2.x = cvRound(x0 - 1000*(-b));
			pt2.y = cvRound(y0 - 1000*(a));
			cvLine( color_dst, pt1, pt2, CV_RGB(255,0,0), 3, 8 );
		log.close();
	else:
		lines = cvHoughLines2( dst, storage, CV_HOUGH_PROBABILISTIC, 1, CV_PI/180, 50, 50, 10 );
		for line in lines:
			cvLine( color_dst, line[0], line[1], CV_RGB(255,0,0), 3, 8 );


	lines = cvHoughLines2( dst, storage, CV_HOUGH_STANDARD, 1, CV_PI/180, 100, 0, 0 );
	print lines;
	log = file(r"/tmp/log.txt", "a+");
	log.write(filename + '\n');
	for i in range(min(lines.total, 100)):
		line = lines[i]
		rho = line[0];
		theta = line[1];
		degrees = theta * (180/CV_PI);
		log.write(str(degrees) + '\n');
		if degrees > 0 and degrees < 45 :
			graus[0] = graus[0]+1;
		if degrees > 45 and degrees < 90 :
			graus[1] = graus[1]+1;
		if degrees > 90 and degrees < 135 :
			graus[2] = graus[2]+1;
		else:
			graus[3] = graus[3]+1;
		total = graus[0] + graus[1] + graus[2] + graus[3];
	log.close();

	print "Arquivo:" + filename;
	for i in range(0, 4):
		porcentagem = ((graus[i] * 100) / total);
		# print "g[" + str(i) + "] " + str(porcentagem) + "\t ", ;

	print "\nimage[" + filename + "] ";
	size = cvGetSize(src);

	# tamanho da largura da imagem dividida em 3
	third_size_width  = (size.width / 3);
	third_size_height  = (size.height / 3);

	# a quarta parte de um terço da largura
	fourth_third_size_width = ( third_size_width / 4 );

	# a quinta parte da altura
	fifth_size_height  = (size.height / 5);

	# debug de tamanho
	# print "\n width [ " + str(size.width) + "] " + " heigh[" + str(size.height) + "] ";
	# print "\n 1/3 width Image [ " + str(third_size_width) + "] " + " 1/4 1/3 [" + str(fourth_third_size_width) + "] ";
	# print "\n 1/5 height Image [" + str(fifth_size_height) + "] ";

	seventh_size_width   = (third_size_width / 7);
	seventh_size_height  = (third_size_height / 7);
	
	# debug de tamanho
	# print "\n 1/7 1/3 width [ " + str(seventh_size_width) + "] " + " 1/7 1/3 heigh[" + str(seventh_size_height) + "] ";

	img = cvLoadImageM(filename)

	# 1st third
	cvRectangle(img, cvPoint(0,0), cvPoint(third_size_width,size.height), cvScalar(0,0,0,0), 2, 8, 0)
	# 3nd third
	cvRectangle(img, cvPoint((2*third_size_width),0), cvPoint(size.width,size.height), cvScalar(0,0,0,0), 2, 8, 0)
	
	# a

	# Abaixo as 4 linhas horizontais na imagem
	# # x1 = (0)
	# # y1 = (0)
	# # x2 = (third_size_width)
	# # y2 = fifth_size_height
	# # cvRectangle(img, cvPoint(x1,y1), cvPoint(x2,y2), cvScalar(30,150,230,0), 2, 8, 0)

	for i in 1,2,3,4: # range(1,5)
		x1 = 0
		y1 = i*fifth_size_height
		x2 = (third_size_width)
		y2 = i*fifth_size_height # *2
		cvRectangle(img, cvPoint(x1,y1), cvPoint(x2,y2), cvScalar(120,190,130,0), 1, 8, 0)

	# Abaixo as 3 linhas verticais na imagem
	for j in 1,2,3: #range(1,4)
		x1 = (j*fourth_third_size_width)
		y1 = 0
		x2 = (j*fourth_third_size_width)
		y2 = size.height 
		cvRectangle(img, cvPoint(x1,y1), cvPoint(x2,y2), cvScalar(100,220,130,0), 1, 8, 0)

	# correction height & width 
	ch  = (seventh_size_height / 2)
	cw  = (seventh_size_width / 2)

	# lembrar que:
	# >>> for i in range(1,2):
	# ...     print i
	# 1
	# >>> 

	Ablocks = list();
	for i in 0,1,2,3,4,5: # range(1,5)
		iablocks = list()
		for j in 0,1,2,3: #range(1,4)
			x1 = (j*fourth_third_size_width - cw)
			y1 = (i*fifth_size_height - ch)
			x2 = (j*fourth_third_size_width + cw)
			y2 = (i*fifth_size_height + ch)
			if ( i == 0 ):
				y1 = 0;
				y2 = (2 * ch);
			if ( i == 5 ):
				y1 = (size.height - (2 * ch))
				y2 = (size.height)
			if ( j == 0 ):
				x1 = 0;
				x2 = 2 * cw;
			cvRectangle(img, cvPoint(x1,y1), cvPoint(x2,y2), cvScalar(0,0,255,0), 1, 8, 0)
			regiao = cvGetSubRect(img, cvRect(x1,y1,(x2-x1),(y2-y1)))
			lum = oper(regiao, (x1,y1,x2,y2))
			# print "A_L [" + str(i) + "," + str(j) + "] = " + str(lum[0])
			iablocks.append(lum[0]);
		Ablocks.append(iablocks);
			
# quadrante C 
			
	# Abaixo as 4 linhas horizontais na imagem
	quadCx = 2*third_size_width

	for i in 1,2,3,4: # range(1,5)
		x1 = quadCx
		y1 = i*fifth_size_height
		x2 = quadCx + (third_size_width)
		y2 = i*fifth_size_height 
		cvRectangle(img, cvPoint(x1,y1), cvPoint(x2,y2), cvScalar(160,95,13,0), 1, 8, 0)

	# Abaixo as 3 linhas verticais na imagem

	for j in 1,2,3: #range(1,4)
		x1 = quadCx + (j*fourth_third_size_width)
		y1 = 0
		x2 = quadCx + (j*fourth_third_size_width)
		y2 = size.height 
		cvRectangle(img, cvPoint(x1,y1), cvPoint(x2,y2), cvScalar(160,96,13,0), 1, 8, 0)

	# correction height & width 
	ch  = (seventh_size_height / 2)
	cw  = (seventh_size_width / 2)

	Cblocks = list();
	for i in 0,1,2,3,4,5: # range(0,6)
		icblocks = list();
		for j in 1,2,3,4: #range(1,5)
			x1 = quadCx + (j*fourth_third_size_width - cw)
			y1 = (i*fifth_size_height - ch)
			x2 = quadCx + (j*fourth_third_size_width + cw)
			y2 = (i*fifth_size_height + ch)
			if ( i == 0 ):
				y1 = 0;
				y2 = (2 * ch);
			if ( i == 5 ):
				y1 = (size.height - (2 * ch))
				y2 = (size.height)
			if ( j == 4 ):
				x1 = size.width - (2 * cw);
				x2 = size.width;
			cvRectangle(img, cvPoint(x1,y1), cvPoint(x2,y2), cvScalar(0,255,0,0), 1, 8, 0)
			regiao = cvGetSubRect(img, cvRect(x1,y1,(x2-x1),(y2-y1)))
			lum = oper(regiao, (x1,y1,x2,y2))
			# print lum
			# print "C_L [" + str(i) + "," + str(j) + "] = " + str(lum[0])
			icblocks.append(lum[0])
		Cblocks.append(icblocks)

	print Ablocks;
	print "Ablocks"
	for i in 0,1,2,3,4,5: # range(1,5)
		for j in 0,1,2,3: #range(1,4)
			print "[" + str(i) + "," + str(j) +"] " +  str(Ablocks[i][j]) + "\t",
		print;
		

	print "Cblocks"
	for i in 0,1,2,3,4,5: # range(1,5)
		for j in 0,1,2,3: #range(1,4)
			print "[" + str(i) + "," + str(j) +"] " +  str(Cblocks[i][j]) + "\t",
		print;

	# criar uma lista de listas e colocar dentro de um for
	# para organizar e fazer as somas

	# pontos = list();
	# for i in 0,1,2,3:
	#     for j in 0,1,2,3,4,5:
	#         preencher  quadrada com pontos certos #A/

	# listas de elementos a serem comparados 
	o123   = list();
	n456   = list();
	aei    = list();
	oabcdn = list();
	bfj    = list();
	oefghn = list();
	cgk    = list();
	oijkln = list();
	dhl    = list();

	o321   = list();
	n654   = list();
	iea    = list();
	ndcbao = list();
	jfb    = list();
	nhgfeo = list();
	kgc    = list();
	nlkjio = list();
	lhd    = list();

	# verificar !!

	for i in 0,1,2,3:
		o123.append(Ablocks[0][i])
		aei.append(Ablocks[1][i])
		bfj.append(Ablocks[2][i])
		cgk.append(Ablocks[3][i])
		dhl.append(Ablocks[4][i])
		n456.append(Ablocks[5][i])
		o321.append(Cblocks[0][i])
		iea.append(Cblocks[1][i])
		jfb.append(Cblocks[2][i])
		kgc.append(Cblocks[3][i])
		lhd.append(Cblocks[4][i])
		n654.append(Cblocks[5][i])

	for i in 0,1,2,3,4,5:
		oabcdn.append(Ablocks[i][0])
		oefghn.append(Ablocks[i][1])
		oijkln.append(Ablocks[i][2])
		ndcbao.append(Cblocks[i][0])
		nhgfeo.append(Cblocks[i][1])
		nlkjio.append(Cblocks[i][2])

	# medias de cada coluna

	mo123 =  sum(o123) / len(o123)
	mn456 =  sum(n456) / len(n456)
	maei =  sum(aei) / len(aei)
	mbfj =  sum(bfj) / len(bfj)
	mcgk =  sum(cgk) / len(cgk)
	mdhl =  sum(dhl) / len(dhl)

	mo321 =  sum(o321) / len(o321)
	mn654 =  sum(n654) / len(n654)
	miea =  sum(iea) / len(iea)
	mjfb =  sum(jfb) / len(jfb)
	mkgc =  sum(kgc) / len(kgc)
	mlhd =  sum(lhd) / len(lhd)

	moabcdn =  sum(oabcdn) / len(oabcdn)
	moefghn =  sum(oefghn) / len(oefghn)
	moijkln =  sum(oijkln) / len(oijkln)

	mndcbao =  sum(ndcbao) / len(ndcbao)
	mnhgfeo =  sum(nhgfeo) / len(nhgfeo)
	mnlkjio =  sum(nlkjio) / len(nlkjio)
	
	print "Medias A3"
	print "o123 " + str(mo123) + " aei " + str(maei) + " bfj " + str(mbfj) + " cgk " + str(mcgk) + " dhl " + str(mdhl) + " n456 " + str(mn456)

	print "Medias A4"
	print "abcd " + str(moabcdn) + " efgh " + str(moefghn) + " ijkl " + str(moijkln)

	print "Medias C3"
	print "o321 " + str(mo321) + " iea " + str(miea) + " jfb " + str(mjfb) + " kgc " + str(mkgc) + " lhd " + str(mlhd) + " n654 " + str(mn654)

	print "Medias C4"
	print "dcba " + str(mndcbao) + " hgfe " + str(mnhgfeo) + " lkji " + str(mnlkjio)

	grau_rotacao = 0;
	peso = 0;

# testa se as bordas são mais claras
	if ( mo123 > mn456 ) and ( mo321 > mn654 ):
		grau_rotacao = 0;
		peso = 0.9;
	elif ( peso < 0.9 ) and  ( mo123 > mn456 ):
		grau_rotacao = 0;
		peso += 0.10;
	elif ( peso < 0.9 ) and ( mo321 > mn654 ):
		grau_rotacao = 0;
		peso += 0.12;
	# incluir comparação das laterais pra ver se tem muita diferença de cor
	elif ( peso < 0.9 ) and ( moabcdn > mo123 ) and ( moabcdn > mo321 ):
		peso += 0.15;
		grau_rotacao = -90;
	elif ( peso < 0.9 ) and ( moabcdn > mo123 ): # só é maior no quad A tem que ter peso menor e/ou colocar outra restrição
		peso += 0.14;
		grau_rotacao = -90;
	elif ( peso < 0.9 ) and ( mnlkjio > mo321 ) and ( mnlkjio > mo123 ):
		peso += 0.18;
		grau_rotacao = 90;
	elif ( peso < 0.9 ) and ( mnlkjio > mo321 ):
		peso += 0.16;
		grau_rotacao = 90;

	# quem sabe dar pesos para as rotações (0,90 e -90) e depois, comparar qual o maior peso e aplicar a rotação apropriada
	
	print "Sugestão de Rotação " + str(grau_rotacao) + " graus" + " peso: " + str(peso);

	# resta agora comparar bloco a bloco Horizontal (H) com Vertical (A,C)
	# para definir para onde girar a imagem. A maior diferença entre eles vence,
	# indicando para qual lado deve ser girado ?

	cvNamedWindow( "Hough4", 1 );
# para os testes, não vou mostrar
	cvShowImage( "Hough4", img );
	cvSaveImage("imagem-teste.jpg", img );
	cvWaitKey(0);

	pi = Image.open(filename)       # PIL image
	output = pi.rotate(grau_rotacao);
	output.show()
	# foutput = "rot_" + filename
	# output.save(foutput)

	# fazendo a transposicao na mao funciona ok
	# PIL do python -- rotaciona a imagem em 90 counterclockwise
	# pi = Image.open(filename)       # PIL image
	# output = pi.rotate(90); output.show() output.save('imagem-rotacionada.jpg')
